//Async action
//Middleware là lớp chắn giữa Component vs data

import { createAction } from ".";
import { request } from "../../configs/request";
import { SET_COURSES } from "./type";

//truyền vào 1 dispatch , nó tự truyền
export const fetchCourses = (dispatch) => {
  //request ở đây là thằng Axios tạo hàm use chung bên Request.js
  request(
    "http://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01",
    "GET"
  ) 
    .then((res) => {
      console.log(res);
      //dispatch action
      //this.props.dispatch( createAction(type, payload) )
      //thay vì dùng chuỗi giờ ta dùng cái biến (trong file action)
      //nhớ import dô
      // type: SET_COURSES,
      //Cái mình cần đưa cho th Store lưu là cái gì ( danh sách data )
      // payload: res.data,
      dispatch(createAction(SET_COURSES, res.data));
      //mục đích chính là thống nhất với nhau các action lại 1 có type 2 là payload , mặt kết quả vẫn như nhau k ảnh hưởng gì hết
    })
    .catch((err) => {
      console.log(err);
    });
};
