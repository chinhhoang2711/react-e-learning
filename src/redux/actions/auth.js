//CƠ CHẾ ĐĂNG NHẬP

import axios from "axios";
import { createAction } from ".";
import { request } from "../../configs/request";
import { SET_TOKEN } from "../actions/type";

export const signIn = (data) => {
  return (dispatch) => {
    request(
      "http://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      "POST",
      data,
    )
      .then((res) => {
        console.log(res);

        //Lữu Local Storage
        localStorage.setItem("t", res.data.accessToken);

        dispatch(createAction(SET_TOKEN, res.data.accessToken));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

//XSS CROSS SCRIPT ATTACK
