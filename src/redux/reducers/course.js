//Lưu thiểu kiểu object {}

import { SET_COURSES } from "../actions/type";

let initialState = {
  courseList: [],

  //   slectedCourse: null,
  //   courseDetail: null,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_COURSES: {
      state.courseList = payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;
