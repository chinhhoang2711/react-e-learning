import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Signin from "./pages/Signin";
import { createAction } from "./redux/actions";
import { SET_TOKEN } from "./redux/actions/type";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/signin" component={Signin} />
          <Route path="/" component={Home} />
        </Switch>
      </BrowserRouter>
    );
  }

  componentDidMount() {
    const token = localStorage.getItem("t");

    //phải connect thì nó mới cho "this.props.dispatch"
    //Store đổi component render lại
    if (token) {
      this.props.dispatch(createAction(SET_TOKEN, token));
    }
  }
}
export default connect()(App);
