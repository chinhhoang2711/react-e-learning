import { createStore, combineReducers, applyMiddleware } from "redux";
import course from "../redux/reducers/course";
import credentials from "../redux/reducers/credentials";
import thunk from "redux-thunk";

//npm install redux-thunk --save
//thêm 1 hàm từ redux để gắn redux thunk

//create root reducer
const reducer = combineReducers({
  //cố tính đặt trùng tên để khỏi xét bằng
  course,
  credentials,
});

export const store = createStore(
  reducer,
  applyMiddleware(thunk)
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
