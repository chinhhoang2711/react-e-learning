import { Container, Grid, Typography, withStyles } from "@material-ui/core";
import React, { Component } from "react";
import CourseItem from "../../components/CourseItem";
import { style } from "./style";
import { connect } from "react-redux";
import { fetchCourses } from "../../redux/actions/course";

class Home extends Component {
  renderCourse = () => {
    return this.props.courseList.map((item, index) => {
      return (
        <Grid item xs={12} sm={6} md={3} lg={3} xl={3} key={index}>
          <CourseItem item={item} />
        </Grid>
      );
    });
  };

  render() {
    return (
      <div>
        <Typography
          className={this.props.classes.title}
          component="h1"
          variant="h2"
        >
          Danh Sách Khóa Học
        </Typography>

        {/* Breakpoints material (lên xem) lg: 1280px */}
        <Container maxWidth="lg">
          <Grid container spacing={3}>
            {this.renderCourse()}
          </Grid>
        </Container>
      </div>
    );
  }

  //Chạy 1 lần duy nhất
  // chạy sau render
  componentDidMount() {
    //AXIOS GIỜ ĐỂ BÊN MIDDLE ĐỂ CALL
    //bên action course.js
    this.props.dispatch(fetchCourses);
  }

  // axios({}) .then().catch()

  componentWillUnmount() {}
}

const mapStateToProps = (state) => {
  return {
    courseList: state.course.courseList,
  };
};

//HOC
export default connect(mapStateToProps)(withStyles(style)(Home));
