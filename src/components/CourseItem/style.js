export const styles = (theme) => {
  return {
    //class
    //qua kia {this.props.classes.(class)}
    img: {
      height: 250,
    },

    btn: {
      backgroundColor: theme.palette.background.grey.light, 
      "&:hover": {
        backgroundColor: theme.palette.secondary.light,
      },
      "& span": {},

      //breakpoin, repoinsive
      //sm: < 600px
      [theme.breakpoints.down("sm")]: {
        color: theme.palette.text.primary,
      },
      [theme.breakpoints.up("sm")]: {
        color: theme.palette.text.yellow,
      },
    },
  };
};
