import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";
import { styles } from "./style";

//Đặt chuột vào nó rồi Ctrl Space cho nó nhắc lệnh

class CourseItem extends Component {
  render() {
    const { tenKhoaHoc, hinhAnh, moTa } = this.props.item;
    return (
      <Card>
        <CardActionArea>
          <CardMedia
            className={this.props.classes.img}
            image={hinhAnh}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {tenKhoaHoc}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {moTa.substr(0, 30) + "..."}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button className={this.props.classes.btn} variant="contained" color="secondary">
            View Detail
          </Button>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles, {withTheme: true})(CourseItem);
//truyền thêm tham số thứ 2 withTheme ( bên trong styles có đc truy cập vài theme dùng k )
